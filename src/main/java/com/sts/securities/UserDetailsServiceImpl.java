package com.sts.securities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sts.models.User;
import com.sts.services.UserService;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException, DataAccessException {
		log.debug("Finding an user with the username: [{}]", userName);

		User user = null;

		try {
			user = userService.findUserByEmail(userName);

			if (user == null) {
				log.debug("user with the username [{}]  NOT FOUND", userName);
				throw new UsernameNotFoundException("Username not found.");
			}

			log.debug("User with the username [{}] FOUND ", userName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new SecurityUser(user);
	}

}

package com.sts.services;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sts.daos.UserDAO;
import com.sts.models.User;
import com.sts.services.UserService;
import com.sts.utils.SecurityUtil;
	
@Service("userService")
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private SecurityUtil securityUtil;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = {"java.lang.Exception" })
	public void saveUser(User user, Long loggedInUserId) {
		log.debug("Saving an user with the information: {}", user);
		
		boolean isNew = user.getId() == null || user.getId() == 0 ? true : false;
		
		if(isNew){
			user.setPassword(securityUtil.encryptUserPassword(user.getPassword()));
			user.setCreatedBy(loggedInUserId);
			user.setCreatedDate(new Date());
			userDAO.persist(user);
		} else {
			
			User dbUser = userDAO.findById(user.getId()); 
			
			dbUser.setFirstname(user.getFirstname()); 
			dbUser.setLastname(user.getLastname()); 
			dbUser.setEmail(user.getEmail()); 
			
			dbUser.setUpdatedBy(loggedInUserId);
			dbUser.setUpdatedDate(new Date());

			if(!StringUtils.isEmpty(user.getPassword())){
				dbUser.setPassword(securityUtil.encryptUserPassword(user.getPassword()));
			}
			userDAO.persist(dbUser);
		}
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = {"java.lang.Exception" })
	public void deleteUser(Long userId) {
		log.debug("Deleting an user entry with the user id: {}", userId);
		User user = userDAO.findById(userId); 
		userDAO.delete(user);
	}

	@Override
	public User findUserById(Long id) {
		log.debug("Finding an user entry with the user id: {}", id);
		return userDAO.findById(id);
	}

	@Override
	public List<User> findAllUsers() {
		log.debug("Finding all user entries");
		return userDAO.findAll();
	}

	@Override
	public User findUserByEmail(String email) {
		log.debug("Finding an user entry with the email: {}", email);
		return userDAO.findUserByEmail(email);
	}

}

package com.sts.services;

import java.io.Serializable;
import java.util.List;

public interface GenericService<E, PK extends Serializable> {

    void saveOrUpdate(E entity, Long loggedInUserId);

    void delete(PK primaryKey);

    E findById(PK primaryKey);

    List<E> findAll();
}

package com.sts.services;

import java.util.List;

import com.sts.models.User;

public interface UserService {

	public void saveUser(User user, Long loggedInUserId);

	public void deleteUser(Long userId);

	public User findUserById(Long id);

	public List<User> findAllUsers();
	
	public User findUserByEmail(String email);
}

package com.sts.services;

import com.sts.daos.GenericDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

public class GenericServiceImpl<E, PK extends Serializable> implements GenericService<E, PK> {

    public static final Logger logger = LoggerFactory.getLogger(GenericServiceImpl.class);

    private GenericDAO<E, PK> genericDAO;

    @Override
    public void saveOrUpdate(E entity, Long loggedInUserId) {
        genericDAO.persist(entity);
    }

    @Override
    public void delete(PK primaryKey) {
        E entity = genericDAO.findById(primaryKey);
        genericDAO.delete(entity);
    }

    @Override
    public E findById(PK primaryKey) {
        return genericDAO.findById(primaryKey);
    }

    @Override
    public List<E> findAll() {
        return genericDAO.findAll();
    }
}

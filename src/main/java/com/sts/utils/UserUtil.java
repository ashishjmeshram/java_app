package com.sts.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.sts.securities.SecurityUser;

public class UserUtil {

	public static SecurityUser getLoggedInUser() {

		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			return null;
		}

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			return (SecurityUser) principal;
		} else {
			return null;
		}
	}
}

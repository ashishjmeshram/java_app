package com.sts.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ErrorController {

	@RequestMapping(value = "/pageNotFound", method = { RequestMethod.GET, RequestMethod.POST })
	public String pageNotFound() {
		return "pageNotFound";
	}

	@RequestMapping(value = "/error", method = { RequestMethod.GET, RequestMethod.POST })
	public String error() {
		return "error";
	}

	@RequestMapping(value = "/accessDenied", method = { RequestMethod.GET, RequestMethod.POST })
	public String accessDenied() {
		return "accessDenied";
	}
}

package com.sts.daos;

import java.io.Serializable;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.sts.models.User;

@Repository(value = "userDAO")
public class UserDAOImpl extends GenericDAOImpl<User, Serializable> implements UserDAO {

	@Override
	public User findUserByEmail(String email) {
		Query query = getCurrentSession().createQuery("SELECT u FROM User u WHERE u.email = :email");
		query.setParameter("email", email);
		return findOne(query);
	}

}

package com.sts.daos;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class GenericDAOImpl<E, PK extends Serializable> implements GenericDAO<E, PK> {

	private Class<E> persistentClass;

	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public GenericDAOImpl() {
		this.persistentClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Class<E> getPersistentClass() {
		return persistentClass;
	}

	protected Session getCurrentSession() {
		return this.sessionFactory.getCurrentSession();
	}

	@Override
	public void persist(E entity) {
		getCurrentSession().saveOrUpdate(entity);
	}

	@Override
	public void delete(E entity) {
		getCurrentSession().delete(entity);
	}

	@Override
	public E findById(PK primaryKey) {
		E entity;
		entity = (E) getCurrentSession().get(getPersistentClass(), primaryKey);
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> findAll() {
		List<E> list = getCurrentSession().createCriteria(getPersistentClass()).list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E findOne(Query query) {
		E t;
		t = (E) query.uniqueResult();
		return t;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> findMany(Query query) {
		List<E> t;
		t = (List<E>) query.list();
		return t;
	}
}
package com.sts.daos;

import java.io.Serializable;

import com.sts.models.User;

public interface UserDAO extends GenericDAO<User, Serializable> {

	User findUserByEmail(String email);

}

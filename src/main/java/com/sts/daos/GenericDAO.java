package com.sts.daos;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;

public interface GenericDAO<E, PK extends Serializable> {

	void persist(E entity);

	void delete(E entity);

	E findById(PK primaryKey);

	E findOne(Query query);
	
	List<E> findMany(Query query);
	
	List<E> findAll();
}
package com.sts.configs;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DBConfig {

	private static final String HIBERNATE_DIALECT = "hibernate.dialect";
	private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	private static final String HIBERNATE_HBM_2_DDL_AUTO= "hibernate.hbm2ddl.auto";
	private static final String HIBERNATE_NEW_GENERATOR_MAPPINGS = "hibernate.id.new_generator_mappings";
	private static final String HIBERNATE_FORMAT_SQL= "hibernate.format_sql";
	private static final String HIBERNATE_USE_SQL_COMMENTS= "hibernate.use_sql_comments";
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private DataSourceConfig dataSourceConfig; 
	
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean asfb = new LocalSessionFactoryBean();

		asfb.setDataSource(dataSourceConfig.dataSource());
		asfb.setHibernateProperties(getHibernateProperties());
		asfb.setPackagesToScan(new String[] { "com.sts" });

		return asfb;
	}

	@Bean
	public Properties getHibernateProperties() {
		Properties properties = new Properties();

		properties.put("hibernate.dialect", environment.getProperty(HIBERNATE_DIALECT));
		properties.put("hibernate.show_sql", environment.getProperty( HIBERNATE_SHOW_SQL));
		properties.put("hibernate.hbm2ddl.auto", environment.getProperty( HIBERNATE_HBM_2_DDL_AUTO));
		properties.put("hibernate.id.new_generator_mappings", environment.getProperty( HIBERNATE_NEW_GENERATOR_MAPPINGS ));
		properties.put("hibernate.format_sql", environment.getProperty( HIBERNATE_FORMAT_SQL));
		// properties.put("hibernate.use_sql_comments", environment.getProperty( HIBERNATE_USE_SQL_COMMENTS));

		return properties;
	}

	@Autowired
	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager htm = new HibernateTransactionManager();
		htm.setSessionFactory(sessionFactory);
		return htm;
	}

}

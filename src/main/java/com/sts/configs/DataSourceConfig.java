package com.sts.configs;

import javax.sql.DataSource;

public interface DataSourceConfig {
	DataSource dataSource();
}

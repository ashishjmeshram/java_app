package com.sts.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource(RootConfig.APPLICATION_PROPERTIES)
@ComponentScan(basePackages = { "com.sts" })
@Configuration
public class RootConfig {

	public static final String APPLICATION_PROPERTIES = "classpath:application.properties";

}

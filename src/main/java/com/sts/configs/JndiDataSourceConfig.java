package com.sts.configs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiObjectFactoryBean;

@Configuration
@ProdEnvironment
@DevEnvironment
public class JndiDataSourceConfig implements DataSourceConfig {

	@Autowired
	private Environment environment;

	private static final String DATASOURCE_JNDI_NAME = "datasource.jndiName";

	@Bean
	public DataSource dataSource() {
		return (DataSource) new JndiObjectFactoryBean() {
			{
				setExpectedType(DataSource.class);
				setResourceRef(true);
				setJndiName(environment.getProperty(DATASOURCE_JNDI_NAME));

				try {
					afterPropertiesSet();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.getObject();
	}
}

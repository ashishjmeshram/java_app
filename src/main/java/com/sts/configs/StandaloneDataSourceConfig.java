package com.sts.configs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@TestEnvironment
@Configuration
public class StandaloneDataSourceConfig implements DataSourceConfig {

	private static final String DB_URL = "db.url";
	private static final String DB_USERNAME = "db.username";
	private static final String DB_PASSWORD = "db.password";
	private static final String DB_DRIVER_CLASS_NAME ="db.driverClassName" ;

	@Autowired
	private Environment environment;

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		dataSource.setDriverClassName(environment.getProperty(DB_DRIVER_CLASS_NAME));
		dataSource.setUrl(environment.getProperty(DB_URL));
		dataSource.setUsername(environment.getProperty(DB_USERNAME));
		dataSource.setPassword(environment.getProperty(DB_PASSWORD));
		
		return dataSource;
	}
}

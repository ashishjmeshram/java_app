<%@ include file="taglibs.jsp"%>

<sec:authorize access="isAuthenticated()">
	<div class="list-group">
 		<a href="<c:url value='/user?operation=userList'/>" class="list-group-item"><fmt:message key="menu.users"/></a> 
		<a href="<c:url value='/logout'/>" class="list-group-item"><fmt:message key="menu.logout"/></a>
	</div>
</sec:authorize>

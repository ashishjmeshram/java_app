package com.sts.services;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static com.natpryce.makeiteasy.MakeItEasy.with;
import static com.sts.builders.UserMaker.User;
import static com.sts.builders.UserMaker.id;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.sts.daos.UserDAO;
import com.sts.models.User;
import com.sts.services.UserService;
import com.sts.services.UserServiceImpl;
import com.sts.utils.SecurityUtil;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

	@Mock
	private UserDAO userDAO;

	@Mock
	private SecurityUtil securityUtil;

	@InjectMocks
	private UserService userService = new UserServiceImpl();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void saveUser_newUserEntry_shouldSaveUser() {
		User newUser = make(a(User, with(id, 0L)));
		
		when(securityUtil.encryptUserPassword(newUser.getPassword())).thenReturn(newUser.getPassword());

		long loggedInUserId = 2L;

		userService.saveUser(newUser, loggedInUserId);

		ArgumentCaptor<User> toDoArgument = ArgumentCaptor.forClass(User.class);

		verify(userDAO, times(1)).persist(toDoArgument.capture());
		verify(securityUtil, times(1)).encryptUserPassword(newUser.getPassword());

		verifyNoMoreInteractions(userDAO);
		verifyNoMoreInteractions(securityUtil);

		User model = toDoArgument.getValue();

		assertThat(model).isEqualTo(newUser);
	}

	@Test
	public void deleteUser_userEntryFound_shouldDeleteFoundUserEntry() {
		User userToDelete = make(a(User));
		when(userDAO.findById(userToDelete.getId())).thenReturn(userToDelete);

		userService.deleteUser(userToDelete.getId());

		verify(userDAO).findById(userToDelete.getId());
		verify(userDAO).delete(userToDelete);
		verifyNoMoreInteractions(userDAO);
	}

	@Test
	public void findAllUsers_userEntriesFound_shouldReturnListOfAllUsers() {
		User user = make(a(User));
		List<User> expectedUserList = Arrays.asList(user);
		when(userDAO.findAll()).thenReturn(expectedUserList);

		List<User> actualUserList = userService.findAllUsers();

		verify(userDAO).findAll();
		verifyNoMoreInteractions(userDAO);

		assertThat(actualUserList).hasSize(1);
		assertThat(actualUserList).isEqualTo(expectedUserList);
		assertThat(actualUserList).contains(user);
	}

	@Test
	public void findUserById_userEntryFound_shouldReturnFoundUserEntry() {
		User expectedUser = make(a(User));
		when(userDAO.findById(expectedUser.getId())).thenReturn(expectedUser);

		User actualUser = userService.findUserById(expectedUser.getId());

		verify(userDAO).findById(expectedUser.getId());
		verifyNoMoreInteractions(userDAO);
		
		assertThat(actualUser).isEqualTo(expectedUser);
	}

	@Test
	public void findUserByEmail_userEntryFound_shouldReturnFoundUserEntry() {
		User expectedUser = make(a(User));
		when(userDAO.findUserByEmail(expectedUser.getEmail())).thenReturn(expectedUser);

		User actualUser = userService.findUserByEmail(expectedUser.getEmail());

		verify(userDAO).findUserByEmail(expectedUser.getEmail());
		verifyNoMoreInteractions(userDAO);
		
		assertThat(actualUser).isEqualTo(expectedUser);
	}

}

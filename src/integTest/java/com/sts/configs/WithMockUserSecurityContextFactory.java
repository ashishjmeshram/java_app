package com.sts.configs;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static com.natpryce.makeiteasy.MakeItEasy.with;
import static com.sts.builders.UserMaker.User;
import static com.sts.builders.UserMaker.id;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import com.sts.models.User;
import com.sts.securities.SecurityUser;

public class WithMockUserSecurityContextFactory implements WithSecurityContextFactory<WithMockUser> {

	@Override
	public SecurityContext createSecurityContext(WithMockUser customUser) {
		SecurityContext context = SecurityContextHolder.createEmptyContext();
		
		User user = make(a(User, with(id, 1000L)));
		
		SecurityUser principal = new SecurityUser(user); 
		
		Authentication auth = new UsernamePasswordAuthenticationToken(principal, principal.getPassword(), principal.getAuthorities());
		context.setAuthentication(auth);
		return context;
	}
}
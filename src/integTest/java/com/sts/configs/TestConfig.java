package com.sts.configs;

import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.destination.Destination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

@TestEnvironment
@PropertySources({ @PropertySource(TestConfig.TEST_PROPERTIES) })
@ComponentScan(basePackages = { "com.sts" })
@Configuration
public class TestConfig {

	public static final String TEST_PROPERTIES = "classpath:test.properties";

	@Bean
	@Autowired
	public Destination destination(DataSourceConfig dataSourceConfig) {
		return new DataSourceDestination(dataSourceConfig.dataSource());
	}

}
